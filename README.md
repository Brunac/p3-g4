
# Projeto 3 - Grupo 4
### Disciplinas de Aplicações Distribuídas (SI) e Sistemas Distribuídos (EC) do semestre 2017-1

## Sistema de Troca de Mensagens Multiprotocolo

### Membros:

* **Weliton Marcos** - Líder/Desenvolvedor :crown: :computer:

* **Gilmar Bernades** - Desenvolvedor :computer:

* **Yan Rocha** - Documentador :writing_hand:
* **Gerson Rosolim** - Documentador :writing_hand:

# Documentação na Wiki
Consulte nossa Documentação na [Wiki](https://gitlab.com/ad-si-2017-1/p3-g4/wikis/home) !

## Changelog

A nomenclatura das Changelogs ficaram definidas como *número.release.build*.

[***Version 1.0.0***](https://gitlab.com/ad-si-2017-1/p3-g4/tags/Version_v1.0)

* Arquivos iniciais do Projeto 3
* Inicio da Documentação
